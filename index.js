var express = require('express');
const path = require('path');
var axios = require('axios');
const qs = require('querystring');
require('dotenv').config();
var app = express();
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
const port = process.env.PORT || 3000;
const url =
  port !== 3000
    ? `https://spotify-node1.herokuapp.com`
    : `http://localhost:${port}`;
app.get('/', (req, res) => {
  res.render('landing');
  // res.send('<a href="/login"><button>Login with Sporify</button></a>');
});

app.get('/login', (req, res) => {
  var scopes = 'ugc-image-upload user-read-playback-state user-read-email ';
  res.redirect(
    `https://accounts.spotify.com/authorize?response_type=code&client_id=${
      process.env.CLIENT_ID
    }&scope=${encodeURIComponent(scopes)}&redirect_uri=${encodeURIComponent(
      `${url}/auth/redirect`,
    )}`,
  );
});

app.get('/auth/redirect', async (req, res) => {
  // console.log('code\n', req.query.code);
  const JSONData = {
    grant_type: 'authorization_code',
    code: req.query.code,
    redirect_uri: `${url}/auth/redirect`,
  };
  const authtoken = Buffer.from(
    `${process.env.CLIENT_ID}:${process.env.CLIENT_SECRET}`,
  ).toString('base64');
  // console.log('AUTH', authtoken);
  const options = {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${authtoken}`,
    },
    data: qs.stringify(JSONData),
    url: 'https://accounts.spotify.com/api/token',
  };
  try {
    await axios(options);
    res.redirect('/spotify/me');
  } catch (error) {
    res.redirect('/fallback');
  }
});

app.get('/spotify/me', (req, res) => {
  res.send('Logged in');
});

app.get('/fallback', (req, res) => {
  res.render('fallback');
});
app.listen(port, () => {
  console.log(`server running on port ${port}`);
});
